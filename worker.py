"""
Passed through args in a json form
{
    channelName: string,
    authId: string,
    authSecret: string,
    history: string[],
    subreddits: string[]
}
"""

# Imports
import sys
import json
import os
import pymongo
from uploader import upload_video, Platform
from postmetadata import generate_metadata
from scraper import get_story
from surfer import create_video

# Variables
params = json.loads(sys.argv[1].replace('\'', '\"'))
platform = Platform.Youtube
video_path = "video_output.mp4"
minStoryLength = 2000
maxStoryLength = 10000
auth = {
    "id": params["clientId"],
    "secret": params["clientSecret"]
}

openai_token = ""  # PLACEHOLDER FOR REAL TOKEN

def add_to_bot_history(channelName, url):
    client = pymongo.MongoClient(os.environ['MONGO_URI'])
    cfilt = {'channel': channelName}
    client['bot']['youtube'].update_one(cfilt, {"$push": {"history": url}})

# Make call to reddit scraper to get story
url, story = get_story(params["subreddits"], params["history"], min=minStoryLength, max=maxStoryLength)

""" TODO: Implement, will help filter for 'worthy' videos 
url = decide_story(stories[])
"""

# Generate title and description
meta = generate_metadata(story, openai_token)
meta["privacyStatus"] = "public" # ("public", "private", "unlisted")



# Generate video
create_video(script=story, output_path=video_path, eleven_labs_key="0e9c4ff27b8147824e7eeba32843f42c", deepgram_key="7ec8e2c362c9aab9becf5b8a2179c99c4b7cda40", openai_key="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")


# Upload to youtube
upload_video(platform, video_path, meta, auth)

#Update bot history
add_to_bot_history(params['name'], url)
