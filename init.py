import json

from dotenv import load_dotenv
import pymongo
import os
import re

load_dotenv()

variables = ['MONGO_URI']

for variable in variables:
    if variable not in os.environ:
        print(f"Missing required environment variable: {variable}")
    else:
        globals()[variable] = os.getenv(variable)

object_id_pattern = r'\'_id\': ObjectId\(\'[a-fA-F0-9]*\'\), '


# Establish a connection to the MongoDB instance running on localhost at port 27017
# noinspection PyUnresolvedReferences
client = pymongo.MongoClient(MONGO_URI)

# Create or select a database
db = client['bot']

# Create or select a collection within that database
collection = db["youtube"]

# Retrieve data from the collection
results = list(collection.find())

strArr = []
for result in results:
    strArr.append(f'{result}')

strArr = [re.sub(object_id_pattern, '', s) for s in strArr]
strArr = ['"' + s + '"' for s in strArr]

print('[' + ', '.join(strArr) + ']')
